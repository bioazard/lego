# Pixit Challenge 

1- Crop y Zoom para que no se distorsione la imagen al ser agregada 
(ejemplo http://phrogz.net/tmp/canvas_zoom_to_cursor.html) 

2.- Que se vean todos los Nombres de los colores ( solo se ven algunos) 

3.- Que el Archivo exportado sea de 32 en lugar de 16 por cada lado

4.- Que se pueda generar un archivo de 50 por lado ( es para la opción llamada portrait)

---------------------------------------------------------
1- Crop and Zoom to avoid distortion on the image used 
(example http://phrogz.net/tmp/canvas_zoom_to_cursor.html) 

2.- All color names are not showing on the intruction pdf ( they should ) 

3.- Exported template should be 32 legos instead of 16 per side

4.- Exported template for te portrait option should be 50 legos per side.

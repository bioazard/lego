let ALL_BRICKLINK_SOLID_COLORS = [
    {
        name: "White",
        hex: "#ffffff",
        id: 1
    },
    {
        name: "Skin Color", 
        hex: "#EBBFA5",
        id: 2
    },
    {
        name: "Light Yellow", 
        hex: "#FCEF76",
        id: 3
    },
    {
        name: "Neon Blue", 
        hex: "#BC297A",
        id: 4
    },
    {
        name: "Purple", 
        hex: "#DFC590",
        id: 5
    },
    {
        name: "Light Blue", 
        hex: "#C6EAE2",
        id: 6
    },
    {
        name: "Dark Pink", 
        hex: "#FD6483",
        id: 7
    },
    {
        name: "Vanilla", 
        hex: "#DEC48F",
        id: 8
    },
    {
        name: "Lavender Purple", 
        hex: "#BFAED9",
        id: 9
    },
    {
        name: "Magenta", 
        hex: "#FD627B",
        id: 10
    },
    {
        name: "Yellow", 
        hex: "#FDD302",
        id: 11
    },
    {
        name: "Dark Brown", 
        hex: "#71473B",
        id: 12
    },
    {
        name: "Blue", 
        hex: "#0165B1",
        id: 13
    },
    {
        name: "Brown", 
        hex: "#AC5725",
        id: 14
    },
    {
        name: "Dark skin Color", 
        hex: "#EE9F7D",
        id: 15
    },
    {
        name: "Pastel Green", 
        hex: "#729881",
        id: 16
    },
    {
        name: "Olive Green",
        hex: "#8C8F62",
        id: 17
    },
    {
        name: "Navy Blue",
        hex: "#7B91A3",
        id: 18
    },
    {
        name: "Dark Blue",
        hex: "#23415C",
        id: 19
    },
    {
        name: "Light Skin Color",
        hex: "#FDD7C1",
        id: 20
    },
    {
        name: "Pink",
        hex: "#D660A4",
        id: 21
    },
    {
        name: "Black",
        hex: "#212121",
        id: 22
    },
    {
        name: "Green",
        hex: "#03A44C",
        id: 23
    },
    {
        name: "Dark Green",
        hex: "#144E40",
        id: 24
    },
    {
        name: "Light Orange",
        hex: "#FFAE13",
        id: 25
    },
    {
        name: "Red Wine",
        hex: "#873432",
        id: 26
    },
    {
        name: "Red",
        hex: "#CB2829",
        id: 27
    },
    {
        name: "Medium Blue",
        hex: "#5AA2DA",
        id: 28
    },
    {
        name: "Lime",
        hex: "#4B4B4F",
        id: 29
    },
    {
        name: "Light Brown",
        hex: "#B47D51",
        id: 30
    },
    {
        name: "Reddish Brown",
        hex: "#513E38",
        id: 31
    },
    {
        name: "Dark Gray",
        hex: "#F0A7D5",
        id: 32
    },
    {
        name: "Peach Skin Color",
        hex: "#DF8E62",
        id: 33
    },
    {
        name: "Sky Blue",
        hex: "#94BAE7",
        id: 34
    },
    {
        name: "Light Pink",
        hex: "#EBA9D5",
        id: 35
    },
    {
        name: "Orange",
        hex: "#FF7C32",
        id: 36
    },
    {
        name: "Light Gray",
        hex: "#A1A9A9",
        id: 37
    },
    {
        name: "Mouse Gray",
        hex: "#707175",
        id: 38
    },
    {
        name: "Dark Pourple",
        hex: "#8053AF",
        id: 39
    },
    {
        name: "Bronze", 
        hex: "#9A866A",
        id: 40
    },    
];

const HEX_TO_COLOR_NAME = {};
ALL_BRICKLINK_SOLID_COLORS.forEach(color => {
    HEX_TO_COLOR_NAME[color.hex] = color.name;
});

const COLOR_NAME_TO_ID = {};
ALL_BRICKLINK_SOLID_COLORS.forEach(color => {
    COLOR_NAME_TO_ID[color.name] = color.id;
});

const KNOWN_BRICKLINK_STUD_COLOR_NAMES = [
    "White",
    "Skin Color",
    "Light Yellow", 
    "Neon Blue", 
    "Purple", 
    "Light Blue", 
    "Dark Pink", 
    "Vanilla", 
    "Lavender Purple", 
    "Magenta", 
    "Yellow", 
    "Dark Brown", 
    "Blue", 
    "Brown", 
    "Dark skin Color", 
    "Pastel Green", 
    "Olive Green",
    "Navy Blue",
    "Dark Blue",
    "Light Skin Color",
    "Pink",
    "Black",
    "Green",
    "Dark Green",
    "Light Orange",
    "Red Wine",
    "Red",
    "Medium Blue",
    "Lime",
    "Light Brown",
    "Reddish Brown",
    "Dark Gray",
    "Peach Skin Color",
    "Sky Blue",
    "Light Pink",
    "Orange",
    "Light Gray",
    "Mouse Gray",
    "Dark Pourple",
    "Bronze", 
];

const KNOWN_BRICKLINK_TILE_COLOR_NAMES = [
    "White",
    "Skin Color", 
    "Light Yellow", 
    "Neon Blue", 
    "Purple", 
    "Light Blue", 
    "Dark Pink", 
    "Vanilla", 
    "Lavender Purple", 
    "Magenta", 
    "Yellow", 
    "Dark Brown", 
    "Blue", 
    "Brown", 
    "Dark skin Color", 
    "Pastel Green", 
    "Olive Green",
    "Navy Blue",
    "Dark Blue",
    "Light Skin Color",
    "Pink",
    "Black",
    "Green",
    "Dark Green",
    "Light Orange",
    "Red Wine",
    "Red",
    "Medium Blue",
    "Lime",
    "Light Brown",
    "Reddish Brown",
    "Dark Gray",
    "Peach Skin Color",
    "Sky Blue",
    "Light Pink",
    "Orange",
    "Light Gray",
    "Mouse Gray",
    "Dark Pourple",
    "Bronze", 
];

const BRICKLINK_STUD_COLORS = ALL_BRICKLINK_SOLID_COLORS.filter(color =>
    KNOWN_BRICKLINK_STUD_COLOR_NAMES.includes(color.name)
).sort((a, b) => {
    return a.name > b.name ? 1 : -1;
});

const BRICKLINK_TILE_COLORS = ALL_BRICKLINK_SOLID_COLORS.filter(color =>
    KNOWN_BRICKLINK_TILE_COLOR_NAMES.includes(color.name)
).sort((a, b) => {
    return a.name > b.name ? 1 : -1;
});

const ADDITIONAL_COLORS = ["Very Light Gray"];
let ALL_VALID_BRICKLINK_COLORS = ALL_BRICKLINK_SOLID_COLORS.sort((a, b) => {
    return a.name > b.name ? 1 : -1;
});
// .filter(
//     color =>
//         KNOWN_BRICKLINK_STUD_COLOR_NAMES.includes(color.name) ||
//         KNOWN_BRICKLINK_TILE_COLOR_NAMES.includes(color.name) ||
//         ADDITIONAL_COLORS.includes(color.name)
// ).sort((a, b) => {
//     return a.name > b.name ? 1 : -1;
// });

const BRICKLINK_PART_OPTIONS = [
    {
        name: "1x1 Round Plate - Default",
        number: 4073
    },
    {
        name: "1x1 Round Tile",
        number: 98138
    },
    {
        name: "1x1 Square Plate",
        number: 3024
    },
    {
        name: "1x1 Square  Tile",
        number: "3070b"
    },
    {
        name: "1x1 Square  Brick",
        number: 3005
    }
];
